import React from "react";
import "./App.css";
import { ChatEngine } from "react-chat-engine";
import ChatFeed from "./Components/ChatFeed";

function App() {
  return (
    <ChatEngine
      height="100vh"
      projectID="960de7c1-10a0-49aa-8102-473d4b09e8ed"
      userName="shady"
      userSecret="123123"
      renderChatFeed={(chatAppProps) => <ChatFeed {...chatAppProps} />}
    />
  );
}

export default App;
