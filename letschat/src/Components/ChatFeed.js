import React from "react";
import MyMessage from "./MyMessage";
import Messageform from "./MessageForm";
import TheirMessage from "./TheirMessage";

function ChatFeed(props) {
  // console.log(props);
  const { chats, activeChat, messages, userName } = props;
  const chat = chats && chats[activeChat];
  // console.log(chat, messages, userName);
  //to display the readmessage
  const renderReadReciepts = (message, isMyMessage) => {
    //going through all the chats by mapping
    return chat.people.map((person, index) => {
      person.last_read === message.id && (
        <div
          className="read-receipt"
          key={`read_${index}`}
          style={{
            backgroundImage: `url(${person?.person?.avatar})`,
            float: isMyMessage ? "right" : "left",
          }}
        ></div>
      );
    });
  };
  //this is to render all the messages
  const renderMessages = () => {
    const keys = Object.keys(messages);
    return keys.map((key, index) => {
      const message = messages[key];
      const lastMessageKey = index === 0 ? null : keys[index - 1];
      const isMyMessage = userName === message.sender.username;
      return (
        <div key={`msg_${index}`} style={{ width: "100%" }}>
          <div className="message-block">
            {isMyMessage ? (
              <MyMessage message={message} />
            ) : (
              <TheirMessage
                message={message}
                lastMessage={message[lastMessageKey]}
              />
            )}
          </div>
          <div
            className="read-reciepts"
            style={{
              marginRight: isMyMessage ? "18px" : "0",
              marginLeft: isMyMessage ? "0px" : "68px",
            }}
          >
            {renderReadReciepts(message, isMyMessage)}
          </div>
        </div>
      );
    });
    // console.log(keys);
  };
  // renderMessages();

  if (!chat) return "loading....";
  return (
    <div className="chat-feed">
      <div className="chat-title-container">
        <div className="chat-title">{chat.title}</div>
        <div className="chat-subtitle">
          {chat.people.map((person) => `${person.person.username}`)}
        </div>
      </div>
      {renderMessages()}
      <div style={{ height: "100px" }}></div>
      <div className="message-form-container">
        <Messageform {...props} chatId={activeChat} />
      </div>
    </div>
  );
}

export default ChatFeed;
