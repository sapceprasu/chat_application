import React, { useState } from "react";

function Loginform() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  
  return (
    <>
      <div className="wrapper">
        <div className="form">
          <h1 className="title">
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                value={username}
                onChange={(event) => {
                  setUsername(event.target.value);
                }}
                className="input"
                placeholder="username"
                required
              />
              <input
                type="password"
                value={password}
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
                className="input"
                placeholder="password"
                required
              />
              <div align="center">
                <button type="submit" className="button ">
                  LOGIN
                </button>
              </div>
            </form>
          </h1>
        </div>
      </div>
    </>
  );
}

export default Loginform;
