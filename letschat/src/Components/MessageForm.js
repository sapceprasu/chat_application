import React from "react";
import { useState } from "react";
import { sendMessage, isTyping } from "react-chat-engine";
import { SendOutlined, PictureOutlined } from "@ant-design/icons";

function MessageForm(props) {
  const [value, setValue] = useState("");

  const { chatId, creds } = props;

  const handleChange = (event) => {
    setValue(event.target.value);
    isTyping(props, chatId);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const text = value.trim();
    if (text?.length > 0) {
      sendMessage(creds, chatId, { text });
    }
    setValue(" ");
  };
  const handleUpload = (event) => {
    // event.preventDefault();
    // const filer = event.target.filer;
    // console.log(filer);
    sendMessage(creds, chatId, { flies: event.target.flies, text: "" });
  };
  console.log(value);
  return (
    <>
      <form className="message-form" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="send a message"
          value={value}
          onChange={handleChange}
          onSubmit={handleSubmit}
          className="message-input"
        />
        <label htmlFor="upload-button">
          <span className="image-button">
            <PictureOutlined
              className="picture-icon"
              style={{ color: "blue" }}
            ></PictureOutlined>
          </span>
        </label>
        <input
          type="file"
          multiple={false}
          id="upload-button"
          style={{ display: "none" }}
          onChange={handleUpload}
        ></input>
        <button type="submit" className="send-buttton">
          <SendOutlined
            className="send-icon"
            style={{ float: "left", color: "blue" }}
          ></SendOutlined>
        </button>
      </form>
    </>
  );
}

export default MessageForm;
