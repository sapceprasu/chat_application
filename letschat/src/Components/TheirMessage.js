import React from "react";

function TheirMessage(props) {
  const { message, lastMessage } = props;
  const isFirstMessageByUser =
    !lastMessage || lastMessage.sender.username !== message.sender.username;

  return (
    <div className="message-row">
      {isFirstMessageByUser && (
        <div
          className="message-avatar"
          style={{ backgroundImage: `url(${message?.sender?.avatar})` }}
        ></div>
      )}
      {message?.attachment?.length > 0 ? (
        <img
          src={message?.attachment[0].file}
          alt="message-sent-loading"
          className="message-image"
          style={{ marginLeft: isFirstMessageByUser ? "4px" : "48px" }}
        />
      ) : (
        <div
          className="message"
          style={{ float: "left", backgroundColor: "#3423fs" }}
        >
          {message.text}
        </div>
      )}
    </div>
  );
}

export default TheirMessage;
